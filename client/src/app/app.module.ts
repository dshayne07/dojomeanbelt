import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { UserService } from './user/user.service';
import { QuestionService } from './question/question.service';
import { DashComponent } from './dash/dash.component';
import { QuestionComponent } from './question/question.component';
import { NewQComponent } from './new-q/new-q.component';
import { AnswerComponent } from './answer/answer.component';
import { SearchPipe } from './search.pipe';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    DashComponent,
    QuestionComponent,
    NewQComponent,
    AnswerComponent,
    SearchPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    UserService,
    QuestionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
