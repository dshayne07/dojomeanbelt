import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';
import { QuestionService } from '../question/question.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
export class DashComponent implements OnInit {
  private user:any;
  private questions:any;
  private searchText:String;

  constructor(private _us:UserService, private _qs:QuestionService, private _router:Router) { }

  ngOnInit() {
    this.session();
    this.allQs();
    this.user = {};
    this.questions = [];
    this.searchText = "";
  }

  session(){
    this._us.session(data=>{
      if(data.errors){
        this._router.navigate(['/register']);
      }else{
        this.user = data;
      }
    })
  }

  allQs(){
    this._qs.allQs(data=>{
      if(data.errors){
        console.log(data.errors);
      }else{
        this.questions = data;
      }
    })
  }

  logout(){
    this._us.logout(data=>{
      this._router.navigate(['/register']);
    })
  }
}
