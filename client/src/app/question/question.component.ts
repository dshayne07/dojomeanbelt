import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';
import { QuestionService } from '../question/question.service';
import { Router, ActivatedRoute, Params } from "@angular/router";

@Component({
  selector: 'QuestionComponent',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  private ques:any;
  private id:any;
  constructor(private _us:UserService, private _qs:QuestionService, private _router:Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.init();
    this.session();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.getQ(this.id);
    });
  }
  
  init(){
    this.ques = {
      question:"",
      description:"",
      answers:[]
    }
  }

  session(){
    this._us.session(data=>{
      if(data.errors){
        this._router.navigate(['/register']);
      }
    })
  }

  logout(){
    this._us.logout(data=>{
      this._router.navigate(['/register']);
    })
    this._router.navigate(['/register']);
  }

  like(id){
    this._qs.like(id, data=>{
      for(var i=0; i<this.ques.answers.length; i++){
        if(this.ques.answers[i]._id == id){
          this.ques.answers[i].likes++;
        }
      }
      this.ques.answers.sort(this.sort);
      console.log(this.ques);
    })
  }

  sort(a, b){
    if (a.likes < b.likes)
      return 1;
    if (a.likes > b.likes)
      return -1;
    return 0;
  }

  getQ(id){
    this._qs.getQ(id, data=>{
      if(data.errors){
        console.log(data.errors);
      }else{
        this.ques = data;
        this.ques.answers.sort(this.sort);
      }
    })
  }

}
