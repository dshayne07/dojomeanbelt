import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class QuestionService {

  constructor(private _http:HttpClient) { }

  createQ(question,cb){
    this._http.post('/server/question/new',question)
    .subscribe(data=>cb(data));
  }

  getQ(id,cb){
    this._http.get('/server/question/'+id)
    .subscribe(data=>cb(data));
  }

  allQs(cb){
    this._http.get('/server/question/')
    .subscribe(data=>cb(data));
  }

  createA(answer,cb){
    this._http.post('/server/answer',answer)
    .subscribe(data=>cb(data));
  }

  like(id,cb){
    this._http.get('/server/answer/like/'+id)
    .subscribe(data=>cb(data));
  }

}
