import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';
import { QuestionService } from '../question/question.service';
import { Router, ActivatedRoute, Params } from "@angular/router";

@Component({
  selector: 'AnswerComponent',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {
  private ques:any;
  private id:any;
  private ans:any;
  constructor(private _us:UserService, private _qs:QuestionService, private _router:Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.init();
    this.session();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.ans['_question'] = this.id;
      this.getQ(this.id);
    });
  }
  
  init(){
    this.ques = {
      question:"",
      description:"",
      answers:[]
    }
    this.ans = {
      answer:"",
      details:"",
      _question:"",
      likes:0
    }
  }

  session(){
    this._us.session(data=>{
      if(data.errors){
        this._router.navigate(['/register']);
      }
    })
  }

  logout(){
    this._us.logout(data=>{
      this._router.navigate(['/register']);
    })
    this._router.navigate(['/register']);
  }

  getQ(id){
    this._qs.getQ(id, data=>{
      if(data.errors){
        console.log(data.errors);
      }else{
        this.ques = data;
      }
    })
  }

  create(){
    this._qs.createA(this.ans, data=>{
      if(data.errors){
        console.log(data.errors);
      }else{
        this._router.navigate(['/']);
      }
    })
  }

}