import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';
import { QuestionService } from '../question/question.service';
import { Router } from "@angular/router";

@Component({
  selector: 'NewQComponent',
  templateUrl: './new-q.component.html',
  styleUrls: ['./new-q.component.css']
})
export class NewQComponent implements OnInit {
  private ques:any;

  constructor(private _us:UserService, private _qs:QuestionService, private _router:Router) { }

  ngOnInit() {
    this.init();
    this.session();
  }

  init(){
    this.ques = {
      question:"",
      description:"",
    }
  }

  session(){
    this._us.session(data=>{
      if(data.errors){
        this._router.navigate(['/register']);
      }
    })
  }


  create(){
    this._qs.createQ(this.ques, data=>{
      if(data.errors){
        console.log(data.errors);
      }else{
        this._router.navigate(['/']);
      }
    })
  }

  logout(){
    this._us.logout(data=>{
      this._router.navigate(['/register']);
    })
    this._router.navigate(['/register']);
  }

}
