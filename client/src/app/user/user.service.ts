import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService {

  constructor(private _http:HttpClient) { }
  
  register(user,cb){
    this._http.post('/server/users/register',user)
    .subscribe(data=>cb(data));
  }

  login(user, cb){
    this._http.post('/server/users/login',user)
    .subscribe(data=>cb(data));
  }

  logout(cb){
    this._http.get('/server/users/logout')
    .subscribe(data=>cb(data));
  }

  session(cb){
    this._http.get('/server/users/session')
    .subscribe(data=>cb(data));
  }
}