import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { Router } from "@angular/router";

@Component({
  selector: 'UserComponent',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  private reg:any;
  private log:any;
  constructor(private _us:UserService, private _router:Router) { }

  init(){
    this.reg = {
      firstName:"",
      lastName:"",
      email:"",
      password:"",
      confirm:""
    }
    this.log = {
      email:"",
      password:""
    }
  }

  ngOnInit() {
    this.init();
  }

  register(){
    this._us.register(this.reg,(data)=>{
      if(data.errors){
        console.log(data.errors);
      }
      else{
        this._router.navigate(['/']);
      }
    });
  }

  login(){
    this._us.login(this.log,(data)=>{
      if(data.errors){
        console.log(data.errors);
      }
      else{
        this._router.navigate(['/']);
      }
    });
  }
}
