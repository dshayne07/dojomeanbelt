import { UserComponent } from './user/user.component';
import { DashComponent } from './dash/dash.component';
import { NewQComponent } from './new-q/new-q.component';
import { QuestionComponent } from './question/question.component';
import { AnswerComponent } from './answer/answer.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path:'new_question', pathMatch:'full', component:NewQComponent },
  { path:'question/:id', component:QuestionComponent },
  { path:'question/:id/new_answer', component:AnswerComponent },
  { path:'register', pathMatch:'full', component:UserComponent },
  { path:'', pathMatch:'full', component:DashComponent },
  { path:'**', redirectTo:'/register' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
